    <h2 class="content-subhead">Consulta de convênios</h2>

    <form class="pure-form pure-form-stacked">

        <fieldset>
            <legend></legend>

            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="first-name">First Name</label>
                    <input id="first-name" name="first-name" class="pure-u-23-24" type="text">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="last-name">Last Name</label>
                    <input id="last-name" class="pure-u-23-24" type="text">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="email">E-Mail</label>
                    <input id="email" class="pure-u-23-24" type="email" >
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="city">City</label>
                    <input id="city" class="pure-u-23-24" type="text">
                    <input id="city-val" type="hidden" value="dd">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="state">State</label>
                    <select id="state" name="state" class="pure-input-1-2">
                        <option>AL</option>
                        <option>CA</option>
                        <option>IL</option>
                    </select>
                </div>

            </div>
        </fieldset>

        <div class="pure-g">
            <div class="pure-u-2-5">
                <button class="pure-button pure-input-1-3">Limpar</button>
                <button class="pure-button pure-button-primary pure-input-1-2">Buscar</button>
            </div>                
            <div class="pure-u-2-5">
                &nbsp;
            </div>                
            <div class="pure-u-1-5">
                <select id="filter-select" name="fff" class="pure-input-1-2">
                    <option>Filtrar por texto</option>
                    <option>Filtro avançado</option>
                </select>                        
                <select id="filter-select" name="fff" class="pure-input-1-2">
                    <option>Filtrar por texto</option>
                    <option>Filtro avançado</option>
                </select>                        
            </div>                
        </div>

        <div class="pure-g">
            <div class="pure-u-1-3">
                <button class="pure-button">Novo</button>
            </div>                
            <div class="pure-u-1-3">
                <button class="pure-button">Impares</button>
                <button class="pure-button">Pares</button>
            </div>                
            <div class="pure-u-1-3">
                <input id="text-search" name="text-search" class="pure-u-1" type="text">
            </div>                
            <div class="pure-u-1">
                <table class="pure-table pure-table-horizontal"  class="pure-u-1" style="width: 100%" >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>Year</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr class="pure-table-odd">
                            <td>1</td>
                            <td>Honda</td>
                            <td>Accord</td>
                            <td>2009</td>
                        </tr>

                        <tr>
                            <td>2</td>
                            <td>Toyota</td>
                            <td>Camry</td>
                            <td>2012</td>
                        </tr>

                        <tr class="pure-table-odd">
                            <td>3</td>
                            <td>Hyundai</td>
                            <td>Elantra</td>
                            <td>2010</td>
                        </tr>

                        <tr>
                            <td>4</td>
                            <td>Ford</td>
                            <td>Focus</td>
                            <td>2008</td>
                        </tr>

                        <tr class="pure-table-odd">
                            <td>5</td>
                            <td>Nissan</td>
                            <td>Sentra</td>
                            <td>2011</td>
                        </tr>

                        <tr>
                            <td>6</td>
                            <td>BMW</td>
                            <td>M3</td>
                            <td>2009</td>
                        </tr>

                        <tr class="pure-table-odd">
                            <td>7</td>
                            <td>Honda</td>
                            <td>Civic</td>
                            <td>2010</td>
                        </tr>

                        <tr>
                            <td>8</td>
                            <td>Kia</td>
                            <td>Soul</td>
                            <td>2010</td>
                        </tr>
                    </tbody>
                </table>                        
            </div>                
            <div class="pure-u-3-4">
                Totais
            </div>                
            <div class="pure-u-1-4">
                <div class="pure-g">
                    <div class="pure-u-1-4">
                        <button class="pure-button"><<</button>
                    </div>
                    <div class="pure-u-1-4">
                        <button class="pure-button"><</button>
                    </div>
                    <div class="pure-u-1-4">
                        <button class="pure-button">></button>
                    </div>
                    <div class="pure-u-1-4">
                        <button class="pure-button">>></button>
                    </div>
                </div>
            </div>                
        </div>

    </form>
    
    <script>

        $("fieldset legend").click(function() {
            if ($(this).parent().children().length == 2)
              $(this).parent().find("div").toggle();
            else
            {
              $(this).parent().wrapInner("<div>");
              $(this).appendTo($(this).parent().parent());
              $(this).parent().find("div").toggle();
            }
        });



        setWrongField("state", "Erro no estado");
        setWrongField("first-name", "Erro no estado")
        setWrongField("cep", "Erro no estado")

    </script>


