<!-- Modal -->
<div class="modal" id="modal-one" style="display:none">
  <div class="modal-dialog open-modal-dialog">
    <div class="modal-header">
      <h2>Modal in CSS?</h2>
      <a id="close-btn" href="#" class="btn-close">×</a> <!--CHANGED TO "#close"-->
    </div>
    <div class="modal-body">
      <p>One modal example here! :D</p>
    </div>
    <div class="modal-footer">
      <a href="#" class="btn">Nice!</a>  <!--CHANGED TO "#close"-->
    </div>
    </div>
  </div>
</div>
<div id="modal-dialog-bg" class="modal-dialog-bg open-modal-dialog"></div>
<!-- /Modal --> 

<script>
    
    // undebugable code
    // TODO: este código deve somente ser uma chamada de função externa
    
    var dialog = $("#modal-one");
    var dialogBg = $("#modal-dialog-bg");
    
    // open
    dialogBg.fadeIn();
    dialog.fadeIn();
    
    // close
    $("#close-btn", dialog).click(function(){
        
        dialog.fadeOut();
        dialogBg.fadeOut();
        
        dialog.remove();
    });
    
    
</script>

