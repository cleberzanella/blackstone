    
    <h2 class="content-subhead">Formulário de convênios</h2>

    <form class="pure-form pure-form-stacked">
        <fieldset>
            <legend>Legend</legend>

            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="first-name">First Name</label>
                    <input id="first-name" name="first-name" class="pure-u-23-24" type="text">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="last-name">Last Name</label>
                    <input id="last-name" class="pure-u-23-24" type="text">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="email">E-Mail</label>
                    <input id="email" class="pure-u-23-24" type="email" >
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="city">City</label>
                    <input id="city" class="pure-u-23-24" type="text">
                    <input id="city-val" type="hidden" value="dd">
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="state">State</label>
                    <select id="state" name="state" class="pure-input-1-2">
                        <option>AL</option>
                        <option>CA</option>
                        <option>IL</option>
                    </select>
                </div>
                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="categories[ ]">Categories</label>
                    <select id="categories" name="categories []" class="pure-u-23-24" multiple="true" size="2">
                        <option>AL</option>
                        <option>CA</option>
                        <option>IL</option>
                        <option>OL</option>
                    </select>
                </div>

                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="city">CEP</label>
                    <div class="pure-g">
                        <input id="cep" name="cep" class="pure-u-18-24" type="text">
                        <span class="pure-u-1-24"></span>
                        <button class="pure-button button-small pure-u-5-24">...</button>
                    </div>
                </div>

            </div>

        </fieldset>
        
            <label for="terms" class="pure-checkbox">
                <input id="terms" type="checkbox"> I've read the terms and conditions
            </label>

            <button type="submit" class="pure-button pure-button-primary">Salvar</button>
            <button class="pure-button">Cancelar</button>

    </form>

<script>

    $("fieldset legend").click(function() {
        if ($(this).parent().children().length == 2)
          $(this).parent().find("div").toggle();
        else
        {
          $(this).parent().wrapInner("<div>");
          $(this).appendTo($(this).parent().parent());
          $(this).parent().find("div").toggle();
        }
    });
    
    
        
    setWrongField("state", "Erro no estado");
    setWrongField("first-name", "Erro no estado")
    setWrongField("cep", "Erro no estado")

</script>

