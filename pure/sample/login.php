<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example with a side menu that hides on mobile, just like the Pure website.">

    <title>Responsive Side Menu &ndash; Layout Examples &ndash; Pure</title>

    


    <link rel="stylesheet" href="css/pure-min.css">
    <link rel="stylesheet" href="css/login.css">


</head>
<body>

<!--  http://codepen.io/SitePoint/pen/rVwJyP -->
    
<div class="c">

  <h2>Login Form with Pure.css</h2>

  <form class="pure-form custom-form" onsubmit="return false;">
    <fieldset>
      <legend>Login Form</legend>

      <input type="email" placeholder="Email">
      <input type="password" placeholder="Password">

      <a class="pure-button" href="index.html">Sign in</a>
    </fieldset>
  </form>

  <p class="p">Demo by Monty Shokeen. <a href="http://www.sitepoint.com/introducing-pure-css-lightweight-responsive-framework" target="_blank">See article</a>.</p>

</div>    
    
</body>
</html>
