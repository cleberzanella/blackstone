<?php

use BlackStone\WebDef\Templating\Template;
use BlackStone\WebDef\TypeCheck\DynamicProxy;
use BlackStone\WebDef\ViewBuilder\FormBuilder;
use BlackStone\WebDef\Model\Pessoa;
use BlackStone\WebDef\ViewBuilder\Layout;

use BlackStone\WebDef\Templating\Basic\FormTemplate;
use BlackStone\WebDef\Templating\Basic\FieldTemplate;
use BlackStone\WebDef\Templating\Basic\TextInputTemplate;
use BlackStone\WebDef\Templating\Basic\SelectTemplate;
use BlackStone\WebDef\Templating\Basic\TextAreaTemplate;
use BlackStone\WebDef\Templating\Basic\ValidationSummaryTemplate;
use BlackStone\WebDef\Templating\Basic\ActionGroupTemplate;
use BlackStone\WebDef\Utilities\ClassFinder;

// lista
$app->path('/teste', function($request) use($app) {
  
    $app->post(function($request) use($app){

        // json (dados)
        $app->format('json', function($request) use($app) {

            return array(
                array("firstName" => "Cleber", "lastName" => "Zanella"),
                array("firstName" => "Bianca", "lastName" => "Ribeiro")
            );
        });
    });
    
   
    $app->get(function($request) use($app) {

       
        // html template
        $app->format('html', function($request) use($app) {

            $c = ClassFinder::read(BULLET_ROOT);
            $cs = implode("; ", $c);
            
            return "<a> Hello <b>World</b> All runtime classes: {$cs} </a>";//$app->template('convenios/list')->set(array('perms' => null));
        });
    });
        
});

$app->path('/template', function($request) use($app) {
  
    $app->get(function($request) use($app) {
        
        // html template
        $app->format('html', function($request) use($app) {
            
            $view = new Template();
            
            $view->title = "hello, world"; 
            $view->links = array("one", "two", "three"); 
            $view->body = "Hi, sup"; 
            $view->content = $view->render(__DIR__ . '/templates/content.php'); 

            return $view->render(__DIR__ . '/templates/main.php');
        });
    });
        
});

$app->path('/template2', function($request) use($app) {
  
    $app->get(function($request) use($app) {
        
        // html template
        $app->format('html', function($request) use($app) {
            
            $proxy = DynamicProxy::spy(Pessoa::ClassName);
            
            //$proxy = new PropertyCallSpyProxy("BlackStone\WebDef\TypeCheck\PropertyCallInfo");

            $proxy->name = "";
            
            return $proxy->lastCalledProperty." ".$proxy->name;
        });
    });
        
});

$app->path('/anfunction', function($request) use($app) {
  
    $app->get(function($request) use($app) {
        
        // html template
        $app->format('html', function($request) use($app) {
            
            $anFn = function(Pessoa $p){ return $p->category;};
            
            $reflector = new ReflectionFunction($anFn);
            
            $typeS = "No";
            
            foreach ($reflector->getParameters() as $param) {
                // param name
                $param->name;

                // param type hint (or null, if not specified).
                $typeS = $param->getClass()->name;

                // finds out if the param is required or optional
                $param->isOptional();
            }
            
            return $typeS;
        });
    });
        
});


$app->path('/template3', function($request) use($app) {
  
    $app->get(function($request) use($app) {
        
        // html template
        $app->format('html', function($request) use($app) {
          
            $builder = new FormBuilder();
            
//            $builder->addDomain(Pessoa::ClassName)->alias("p");
//            $builder->addDomain(Endereco::ClassName)->alias("e")->association("enderecoPessoa");
            
            $builder->regiterTemplates(array(
//                new TextBoxTemplate(),
//                new ComboBoxTemplate(),
//                new TextAreaTemplate()
            ));

            $builder->beginGroup(Layout::FullWidth);
                $builder->addValidationSummary();
            $builder->endGroup();
            
            $builder->beginGroup(Layout::TwoCols);
                $builder->addFieldFn(function(Pessoa $p){ return $p->name;}); // (Person $p) $p->name
                $builder->addFieldFn(function(Pessoa $p){ return $p->category;})
                        ->viewAs("combo", array(1 => "Student", 2 => "Customer"));
            $builder->endGroup();

            $builder->beginGroup(Layout::FullWidth);
                $builder->addFieldFn(function(Pessoa $p){ return $p->description;})
                        ->viewAs("textarea", array("lines" => 3));
            $builder->endGroup();
            
            $builder->beginGroup(Layout::FullWidth);
                $builder->addDefaultActions()->alignLeft();
            $builder->endGroup();

            //$builder->data->propName = "dd";
            // Criar a Tag Script, que recebe um JSON que inicializa todos os componentes
            
            return $app->template('webdef/builder')->set(array('builder' => $builder));
            
        });
    });
        
});



$app->path('/templ', function($request) use($app) {
  
    $app->get(function($request) use($app) {
        
        // html template
        $app->format('html', function($request) use($app) {
            
            $formTemplate = new FormTemplate();
            
            $formTemplate->data->action = "zfg";
            $formTemplate->data->method = "POST";

            $formTemplate->setGroup(1);
            {
                $validatoinSummary = new ValidationSummaryTemplate();
                $formTemplate->addChild($validatoinSummary);
            }
            
            $formTemplate->setGroup(2);
            {
                
                $nameField = new FieldTemplate();
                $nameField->data->label = "Nome:";

                $nameText = new TextInputTemplate();
                $nameText->data->name = "firstName";
                $nameText->data->value = "Cleber";
                $nameField->addChild($nameText);
                $formTemplate->addChild($nameField);


                $lastField = new FieldTemplate();
                $lastField->data->label = "Sobrenome:";

                $lastText = new TextInputTemplate();
                $lastText->data->name = "lastName";
                $lastText->data->value = "Zanella";
                $lastField->addChild($lastText);
                $formTemplate->addChild($lastField);

                
                $genderField = new FieldTemplate();
                $genderField->data->label = "Sexo:";

                $genderSelect = new SelectTemplate();
                $genderSelect->data->name = "name";
                $genderSelect->data->multiple = false;
                $genderSelect->data->selectedValue = 2; // não funciona
                $genderSelect->addOption(1, "Feminino");
                $genderSelect->addOption(2, "Masculino", true);
                $genderField->addChild($genderSelect);
                $formTemplate->addChild($genderField);

            }

            $formTemplate->setGroup(1);
            {
                
                $obsField = new FieldTemplate();
                $obsField->data->label = "Anotação:";

                $obsText = new TextAreaTemplate();
                $obsText->data->name = "observacao";
                $obsText->data->value = "Observações aleatória sobre algo.";
                $obsText->data->lines = 3;
                $obsField->addChild($obsText);
                $formTemplate->addChild($obsField);
                
            }
            
            $formTemplate->setGroup(1);
            {
                $actionGroup = new ActionGroupTemplate();
                
                $actionGroup->addAction("submit", "Enviar", "submit");
                $actionGroup->addAction("cancel", "Cancelar");
                
                $formTemplate->addChild($actionGroup);
            }
            
            return $app->template('webdef/content')->set(array('htmlContent' => $formTemplate->render()));
        });
    });
        
});
