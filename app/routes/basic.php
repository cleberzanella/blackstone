<?php

// lista
$app->path('/lista', function($request) use($app) {
  
    $dao = new ConvenioDAO();
    
    $app->post(function($request) use($app, $dao){

        // json (dados)
        $app->format('json', function($request) use($app, $dao) {

            return $dao->buscarConvenios(Convert::paramsToFilter($request->post()));
        });
    });
    
    $app->get(function($request) use($app, $perms) {
        
        // html template
        $app->format('html', function($request) use($app, $perms) {
            
            return $app->template('basic/list')->set(array('perms' => $perms));
        });
    });
        
});


// formulário
$app->path('/form', function($request) use($app) {

    // editar
    $app->param('int', function($request, $itemId) use($app, $validator, $dao, $dataAction, $perms) {
        
        $app->get(function($request) use($app, $dao, $itemId) {
            
            $convenio = $dao->buscarConvenio($itemId);
            
            if($convenio == null){
                return $app->response(404); // Not found
            }

            return  $app->template('basic/form')
                ->set(array('action' => $itemId, 'method' => 'post', 'state' => Convert::sanitizeForView($convenio)))
                ->format('html')
                ->status(200);
        });

        $app->post(function($request) use($app, $itemId, $validator, $dao, $dataAction, $perms) {
            
            $allPostParams = $request->post();
            $convenio = Convert::paramsToConvenio($allPostParams);
            $convenio->id = $itemId;

            return $dataAction($convenio, ConvenioValidator::Update, $dao, $validator, $app, $perms);
        });

        $app->delete(function($request) use($app, $itemId, $validator, $dao, $dataAction, $perms) {
            
            $convenio = new Convenio();
            $convenio->id = $itemId;

            return $dataAction($convenio, ConvenioValidator::Remove, $dao, $validator, $app, $perms);
        });
        
    });

    // criar
    $app->path('/new', function($request) use($app, $dao, $validator, $dataAction, $perms) {

        $app->get(function($request) use($app) {
            
            return  $app->template('basic/form')
                ->set(array('action' => 'new', 'method' => 'put', 'state' => Convert::sanitizeForView(new Convenio())))
                ->format('html')
                ->status(200);
        });

        $app->put(function($request) use($app, $dao, $validator, $dataAction, $perms) {
            
            $allPutParams = $request->params();
            $convenio = Convert::paramsToConvenio($allPutParams);
            
            return $dataAction($convenio, ConvenioValidator::Create, $dao, $validator, $app, $perms);
        });


    });    
});