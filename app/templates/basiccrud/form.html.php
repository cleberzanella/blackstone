<form action="<?php echo $action; ?>" method="<?php echo $method; ?>" id="form-id" class="grid-container align-labels-top fill-inputs-horizontal">
    <div class="fullwidth">
        <div id="messages-container" class="col">
        </div>
    </div>
    <div class="grid2">
        <div class="col">
            <label>Nome:</label>
            <input id="nome-field" name="nome" value="<?php echo $state->nome; ?>">
            <span class="error-label"></span>
        </div>
        
        <div class="col">
            <label>Sobrenome:</label>
            <input id="sobrenome-field" name="sobrenome" value="<?php echo $state->sobrenome; ?>">
            <span class="error-label"></span>
        </div>
        
        <div class="col">
            <label>Estado:</label>
            <select id="sexo-field" name="sexo" defaultvalue="<?php echo $state->sexo; ?>" >
                <option value="1">Feminino</option>
                <option value="2">Masculino</option>
            </select>
            <span class="error-label"></span>
        </div>
    </div>
    <div class="fullwidth">
        <div class="col">
            <label>Observações:</label>
            <textarea id="observacoes-field" name="observacoes" value="Descrição das observações"><?php echo $state->observacoes; ?></textarea>
            <span class="error-label" title="Cleber"></span>
        </div>
    </div>
    
    <div class="fullwidth">
        <div class="col">
            <span>
                <input id="submit" type="submit" value="Enviar"></input>
                <button id="close-btn" type="button">Fechar</button>
            </span>
        </div>
    </div>

</form>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js" ></script>

<!-- Componentes JQuery UI -->
<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js" ></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js" ></script>
