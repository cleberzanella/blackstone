<html>
<head>
  <title>Convênios</title>
  
  <!-- Componente JQuery DataTable -->
  <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet" title="style" media="all">

  <!-- Plugins do JQuery DataTable -->
  <link href="https://cdn.datatables.net/select/1.1.0/css/select.dataTables.min.css" rel="stylesheet" title="style" media="all">
  <link href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" rel="stylesheet" title="style" media="all">

  <!-- Grid Layout para formulários -->
  <link href="css/simple-grid-layout.css" rel="stylesheet" title="style" media="all">
  <link href="css/top-labels-alignment.css" rel="stylesheet" title="style" media="all">
  <link href="css/message-box.css" rel="stylesheet" title="style" media="all">
  
  <!-- Componentes para formulários -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="https://harvesthq.github.io/chosen/chosen.css">
  
</head>
<body>
    
    <?php if(false) {?>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js" ></script>

    <!-- Componentes JQuery UI -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js" ></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js" ></script>

    <script type="text/javascript" src="http://digitalbush.com/wp-content/uploads/2014/10/jquery.maskedinput.js" ></script>
    <script type="text/javascript" src="https://harvesthq.github.io/chosen/chosen.jquery.js"> </script>

    <link rel="stylesheet" href="http://foliotek.github.io/Croppie/croppie.css">
    <script type="text/javascript" src="http://foliotek.github.io/Croppie/croppie.js"> </script>

    <script type="text/javascript" src="../js/convenios.js"> </script>
    
    <?php } ?>
    
  <div>
    <?php echo $yield; ?>
  </div>
    
</body>
</html>

