
<table id="example-table" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Observações</th>
        </tr>
    </thead>
</table>

<p style="font-size:10px;"><i>Dica: Você pode utilizar na busca o nome, endereço ou observações sobre a empresa.</i></p>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js" ></script>

<!-- Componente JQuery DataTable -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.js" ></script>

<!-- Plugins do JQuery DataTable -->
<script type="text/javascript" src="https://cdn.datatables.net/select/1.1.0/js/dataTables.select.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js" ></script>

<script type="text/javascript">

     $(document).ready(function() {

        // Depende de: https://www.datatables.net/

        function getSelectedIds(dataTable){
            
            var selectedIndexes = dataTable.rows( { selected: true } )[0];

            var selectedIds = [];
            var dataTableData = dataTable.data();

            for(var index = 0; index < selectedIndexes.length; index++){
                var selectedIndex = selectedIndexes[index];
                var selectedData = dataTableData[selectedIndex];

                selectedIds.push(selectedData.id);
            }
            
            return selectedIds;
        }

        function getFirstSelectedId(dataTable){
            
            var selectedIds = getSelectedIds(dataTable);

            if(selectedIds.length === 0) {
                return null;
            }
            
            return selectedIds[0];
        }

        var dataTable = $('#example-table').DataTable( {
            processing: true,
            serverSide: true,
            ajax: {
                url: "empresas",
                type: "POST",
                dataType: "json",
            },
            ordering: false,
            columnDefs: [
                {
                    targets: [ 0 ],
                    visible: false
                }
            ],
            columns: [
                { data: "id" },
                { data: "name" },
                { data: "address" },
                { data: "obs" }
            ],
            // habilita a seleção das linhas
            select: {
                style: "multi"
            },            
            // habilita os botões
            dom: 'lBfrtip', // *** esse parâmetro dom é magia negra, mude e saia correndo imediatamente
            buttons: [
<?php if($perms->canInsert()){ ?>
                {
                    text: 'Nova empresa',
                    action: function () {
                        document.location.href = 'empresa/new';
                    }
                },
<?php }
    if($perms->canUpdate()) {
?>
                {
                    extend: "selectedSingle",
                    text: 'Editar',
                    action: function () {
                        
                        var selectedId = getFirstSelectedId(dataTable);

                        if(selectedId !== null) {

                            document.location.href = 'empresa/' + selectedId;
                            return;
                        }
                    }
                },
<?php }
    if($perms->canDelete()){
?>
                {
                    extend: "selected",
                    text: 'Excluir',
                    action: function () {
                        
                        // cria todos os request de uma só vez
                        function requestDeletes(itemsIds){
                            
                            var requests = [];
                            
                            for(var i = 0; i < itemsIds.length; i++){
                                requests.push(
                                            $.ajax({
                                                url: "empresa/" + itemsIds[i],
                                                method: "DELETE",
                                                data:  {}
                                            })
                                        );
                            }
                            
                            return requests;
                        }
                        
                        var selectedIds = getSelectedIds(dataTable);
                        
                        if(selectedIds.length > 0){
                            if(confirm("Confirma a exclusão de " + selectedIds.length + " item(s)?" )){

                                // executa (e espera) todos os request de uma só vez
                                $.when.apply(undefined, requestDeletes(selectedIds)).done(function(){
//                                    // caso algum dia, alguém tenha interesse pelos resultados, basta ler os "arguments"
                                });

                                // remove as linhas 
                                dataTable.row('.selected').remove().draw( true );
                            }
                        }
                    }
                },
<?php } ?>
            ],
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json"
            }
        } );
        
     });

</script>
