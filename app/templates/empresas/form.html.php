<form action="<?php echo $action; ?>" method="<?php echo $method; ?>" id="form-id" class="grid-container align-labels-top fill-inputs-horizontal">
    <div class="fullwidth">
        <div id="messages-container" class="col">
        </div>
    </div>
    <div class="fullwidth">
        <div class="col">
            <label>Nome:</label>
            <input id="nome-field" name="nome" value="<?php echo $state->nome; ?>">
            <span class="error-label"></span>
        </div>
    </div>
    <div class="grid2">
        <div class="col">
            <label>CNPJ:</label>
            <input id="masked-cnpj-field" name="masked-cnpj" value="">
            <input id="cnpj-field" name="cnpj" type="hidden" value="<?php echo $state->cnpj; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>CEP:</label>
            <input id="masked-cep-field" name="masked-cep" value="">
            <input id="cep-field" name="cep" type="hidden" value="<?php echo $state->cep; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>País:</label>
            <select id="pais-field" name="pais" defaultvalue="<?php echo $state->pais; ?>"></select>
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Estado:</label>
            <select id="estado-field" name="estado" defaultvalue="<?php echo $state->estado; ?>" ></select>
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Cidade:</label>
            <select id="cidade-field" name="cidade" defaultvalue="<?php echo $state->cidade; ?>" ></select>
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Endereço:</label>
            <input id="endereco-field" name="endereco" value="<?php echo $state->endereco; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Telefone 1:</label>
            <input id="masked-telefone1-field" name="masked-telefone1" value="">
            <input id="telefone1-field" type="hidden" name="telefone1" value="<?php echo $state->telefone1; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Telefone 2:</label>
            <input id="masked-telefone2-field" name="masked-telefone2" value="">
            <input id="telefone2-field" type="hidden" name="telefone2" value="<?php echo $state->telefone2; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Email 1:</label>
            <input id="email1-field" name="email1" value="<?php echo $state->email1; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Email 2:</label>
            <input id="email2-field" name="email2" value="<?php echo $state->email2; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Categorias:</label>
            <select id="categorias-field" name="categorias[ ]" defaultvalue="<?php echo $state->categorias; ?>" multiple="multiple"></select>
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Web site:</label>
            <input id="website-field" name="website" value="<?php echo $state->website; ?>">
            <span class="error-label"></span>
        </div>
        <div class="col">
            <label>Logo:</label>
            <div id="logomarca-field-container">
                <input id="logomarca-file-field" type="file" id="files" title=" "/>
                <input id="logomarca-field" name="logomarca" type="hidden" value="<?php echo $state->logomarca; ?>">
                <output id="list"></output>
            </div>
            <span class="error-label"></span>
        </div>
    </div>
    <div class="fullwidth">
        <div class="col">
            <label>Observações:</label>
            <textarea id="observacoes-field" name="observacoes" value="Descrição das observações"><?php echo $state->observacoes; ?></textarea>
            <span class="error-label" title="Cleber"></span>
        </div>
    </div>
    
    <div class="fullwidth align-actions-left">
        <div class="col">
            <span>
                <input id="submit" type="submit" value="Enviar"></input>
                <button id="close-btn" type="button">Fechar</button>
            </span>
        </div>
    </div>

</form>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js" ></script>

<!-- Componentes JQuery UI -->
<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js" ></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js" ></script>

<script type="text/javascript" src="http://digitalbush.com/wp-content/uploads/2014/10/jquery.maskedinput.js" ></script>
<script type="text/javascript" src="https://harvesthq.github.io/chosen/chosen.jquery.js"> </script>

<link rel="stylesheet" href="http://foliotek.github.io/Croppie/croppie.css">
<script type="text/javascript" src="http://foliotek.github.io/Croppie/croppie.js"> </script>

<script type="text/javascript" src="../js/convenios.js"> </script>

<script type="text/javascript">

    $(document).ready(function() {
        
        // Inicialização dos componentes de edição
        {
            new MaskedField('cnpj-field', MaskedField.CNPJ_MASK, MaskedField.CNPJ_UNMASK_FN);
            
            new MaskedField('cep-field', MaskedField.ZIP_CODE_MASK, MaskedField.ZI_CODE_UNMASK_FN);

            new MaskedField('telefone1-field', MaskedField.INTERNATIONAL_PHONE_MASK, MaskedField.INTERNATIONAL_PHONE_UNMASK_FN);

            new MaskedField('telefone2-field', MaskedField.INTERNATIONAL_PHONE_MASK, MaskedField.INTERNATIONAL_PHONE_UNMASK_FN);


            var categoriasField = new SmallDataSelectBox("#categorias-field", "categorias", {});
            categoriasField.bindData();

            var paisField = new SmallDataSelectBox("#pais-field", "paises", {});
            paisField.bindData();

            var estadoField = new SmallDataSelectBox("#estado-field", "estados", function() { return { countryId: paisField.getActualValues()}; });
            estadoField.bindData();

            var cidadeField = new SmallDataSelectBox("#cidade-field", "cidades", function() { return { stateId: estadoField.getActualValues()}; });
            cidadeField.bindData();

            // dependência entre campos
            setTimeout(function() {
                // precisa esperar o servidor responder com os dados
                paisField.addDependant(estadoField);
                
                setTimeout(function(){
                    estadoField.addDependant(cidadeField);
                }, 300);

                // TODO : ligar o campo cep ao webservice dos correios: http://cep.correiocontrol.com.br/89190000.json
                // Argentina: http://www.correoargentino.com.ar/formularios/cpa

            }, 400);
            
            var imgSelect = new ImageSelectBox("#logomarca-field", "#logomarca-file-field");
            imgSelect.setFilterFn(ImageSelectBox.cropFilterFn(195,90));
        }

        // Inicialização das validações
        var validation = null;
        {
            var fieldMappings = {
                nome: {
                    valueElementSelector: "#nome-field",
                    editionElementSelector: "#nome-field"
                },
                cnpj: {
                    valueElementSelector: "#cnpj-field",
                    editionElementSelector: "#masked-cnpj-field"
                },
                cep: {
                    valueElementSelector: "#cep-field",
                    editionElementSelector: "#masked-cep-field"
                },
                pais: {
                    valueElementSelector: "#pais-field",
                    editionElementSelector: "#pais_field_chosen .chosen-single"
                },
                estado: {
                    valueElementSelector: "#estado-field",
                    editionElementSelector: "#estado_field_chosen .chosen-single"
                },
                cidade: {
                    valueElementSelector: "#cidade-field",
                    editionElementSelector: "#cidade_field_chosen .chosen-single"
                },
                endereco: {
                    valueElementSelector: "#endereco-field",
                    editionElementSelector: "#endereco-field"
                },
                telefone1: {
                    valueElementSelector: "#telefone1-field",
                    editionElementSelector: "#masked-telefone1-field"
                },
                telefone2: {
                    valueElementSelector: "#telefone2-field",
                    editionElementSelector: "#masked-telefone2-field"
                },
                email1: {
                    valueElementSelector: "#email1-field",
                    editionElementSelector: "#email1-field"
                },
                email2: {
                    valueElementSelector: "#email2-field",
                    editionElementSelector: "#email2-field"
                },
                categorias: {
                    valueElementSelector: "#categorias-field",
                    editionElementSelector: "#categorias_field_chosen .chosen-choices"
                },
                website: {
                    valueElementSelector: "#website-field",
                    editionElementSelector: "#website-field"
                },
                observacoes: {
                    valueElementSelector: "#observacoes-field",
                    editionElementSelector: "#observacoes-field"
                },
                logomarca: {
                    valueElementSelector: "#logomarca-field",
                    editionElementSelector: "#logomarca-field-container"
                },
            };

            validation = new ValidationSummarizer("messages-container", fieldMappings);
        }

        // Inicializa o formulário
        {
            var formController = new FormController("form-id", validation);
            formController.setCloseUrl('../empresas');
        }
        
    });

</script>