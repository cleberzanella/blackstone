<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Model;

/**
 * Description of Pessoa
 *
 * @author user
 */
class Pessoa {
    
    const ClassName = __CLASS__;
    
    /**
     * @var int
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var int
     */
    public $category;

    /**
     *
     * @var string
     */
    public $description;

    public function __construct() {
        
    }
}
