<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\ViewBuilder;

use BlackStone\WebDef\TypeCheck\PropertyCallSpyProxy;
use ReflectionFunction;
use BlackStone\WebDef\TypeCheck\DynamicProxy;

/**
 * Description of Builder
 *
 * @author user
 */
class FormBuilder {
    
    public function regiterTemplates(array $templates){
        
    }

    public function beginGroup($columns){
        
    }

    public function endGroup(){
        
    }

    public function addValidationSummary(){
        
    }

    public function addDefaultActions(){
        return new DefaultActionsBuilder();
    }

    public function addField($className, $fieldName){
        
        $fielfInfo = new FieldInfo($className, $fieldName, PropertyCallSpyProxy::getPropertyType($className, $fieldName));
        
        return new FieldBuilder();
    }
    
    public function addFieldFn($fieldAccessFunction){
        
        $fielfInfo = FieldInfo::fromFn($fieldAccessFunction);
        
        return new FieldBuilder();
    }

    public function render(){
        // Pega as "build-instructions" e cria os templates html e o JSON que descreve o comportamento do JS
        return "<h1> Hello <i>Builder</i>!! </h1>";
    }
    
}

class FieldBuilder {
    
    public function viewAs($componentName, array $componentArguments){
        
    }
    
}

class DefaultActionsBuilder {
    
    public function alignLeft(){
        
    }
    
}

class FieldInfo {
    public $parentClass, $name, $type;
    
    public function __construct($p, $n, $t) {
        $this->parentClass = $p;
        $this->name = $n;
        $this->type = $t;
        
        if(!FieldInfo::fieldExists($p, $n)){
            throw new \Exception("Class " . $p . " not has property " . $n); 
        }
    }
    
    private static function fieldExists($clazz, $fieldName){
        
        $reflector = new \ReflectionClass($clazz);
        
        return $reflector->hasProperty($fieldName);
    }
    
    public static function fromFn($fieldAccessFunction){
        $reflector = new ReflectionFunction($fieldAccessFunction);

        $parentClassName = null;
        $paramName = null;

        foreach ($reflector->getParameters() as $param) {
            $paramName = $param->name;
            
            // param type hint (or null, if not specified).
            $parentClassName = $param->getClass()->name;
        }

        if($parentClassName == null){
            throw new \Exception("Accessor function does not have param type hint specified for param " . $paramName . "."); 
        }
        
        $paramProxy = DynamicProxy::spy($parentClassName);
        
        call_user_func_array($fieldAccessFunction, array($paramProxy));
        
        if($paramProxy->lastCalledProperty == null){
            throw new \Exception("Accessor function body does not call a property of param " . $paramName . "."); 
        }
        
        return new FieldInfo($parentClassName, $paramProxy->lastCalledProperty, null/*type*/);
    }
    
}
