<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\ViewBuilder;

/**
 * Description of Layout
 *
 * @author user
 */
abstract class Layout
{
    const FullWidth = 1;
    const TwoCols = 2;
    const ThreeCols = 2;
    const FourCols = 2;
    const FiveCols = 2;
}
