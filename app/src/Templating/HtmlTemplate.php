<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating;

use BlackStone\WebDef\Templating\Template;

/**
 * Description of HtmlTemplate
 *
 * @author user
 */
class HtmlTemplate {
    
    /**
     *
     * @var HtmlTemplate
     */
    private  $lastChild;


    /**
     *
     * @var array
     */
    private $children = array();
    
    /**
     *
     * @var Model
     */
    public $data = array();
    
    public function __construct() {
        
    }
    
    public function getTemplatePath(){
        $clazz = get_class($this);
        
        $reflector = new \ReflectionClass($clazz);
        $dirName = dirname($reflector->getFileName());
        $templateFileName = str_replace("Template", "", $reflector->getShortName()) . ".html.php";
        
        return "$dirName/$templateFileName";
    }
    
    public function addChild(HtmlTemplate $child){
        $this->lastChild = $child;
        array_push($this->children, $child);
    }
    
    public function getLastChild(){
        return $this->lastChild;
    }
    
    public function render(){
        
        if(!is_array($this->data) && property_exists(get_class($this->data), "htmlContent")){
            $content = "";

            foreach ($this->children as $childTemplate) {
                $content .= $childTemplate->render();
            }

            $this->data->htmlContent = $content;
        }
        
        $template = new Template();
        $template->data = $this->data;
        return $template->render($this->getTemplatePath());
    }
    
}
