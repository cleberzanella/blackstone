<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating;

/**
 * Description of Template
 * Source: http://chadminick.com/articles/simple-php-template-engine.html
 *
 * @author user
 */
class Template { 
    
    private $vars = array(); 
    
    public function __get($name) { 
        return $this->vars[$name]; 
    } 
    
    public function __set($name, $value) { 
        if($name == 'view_template_file') { 
            throw new Exception("Cannot bind variable named 'view_template_file'"); 
        } 
        
        $this->vars[$name] = $value; 
    } 
    
    public function render($view_template_file) { 
        if(array_key_exists('view_template_file', $this->vars)) { 
            throw new Exception("Cannot bind variable called 'view_template_file'"); 
        } 
        extract($this->vars); 
        ob_start(); 
        include($view_template_file); 
        return ob_get_clean(); 
    } 
}