<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

/**
 * Description of TextAreaTemplate
 *
 * @author user
 */
class TextAreaTemplate extends TextInputTemplate {
    
    /**
     *
     * @var TextAreaModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new TextAreaModel();
    }
}
