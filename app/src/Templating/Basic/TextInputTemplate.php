<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of TextBoxTemplate
 *
 * @author user
 */
class TextInputTemplate extends HtmlTemplate {
    
    /**
     *
     * @var TextInputModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new TextInputModel();
    }
    
    public function render() {
        
        $this->data->elementId = "{$this->data->name}-field";
        
        return parent::render();
    }
    
    // Compentes basicos farão parte do "Basic" ou "ZeroDependency" ou "PureHTML"
    
    // declarar um alias (string) e talvez um conjunto de templates. Ex.: JQuerMobile1_5, Bootstrap3, Foundation5
    // receber os dados
    // dar um render
    
}
