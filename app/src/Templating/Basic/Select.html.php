<?php

use BlackStone\WebDef\Templating\Basic\SelectModel;

$d = new SelectModel();
$d = $data;

?>

<select id="<?php echo $d->elementId; ?>" name="<?php echo $d->name; ?>[ ]" defaultvalue="<?php echo $d->selectedValue; ?>" <?php echo $d->multiple ? 'multiple="multiple" size="2"' : ''; ?>>
    <?php foreach($d->options as $option):?>
    <option <?php echo $option->selected ? "selected" : ""; ?> <?php echo $option->disabled ? "disabled" : ""; ?> value="<?php echo $option->value; ?>"><?php echo $option->description; ?></option>
    <?php endforeach;?>
</select>
