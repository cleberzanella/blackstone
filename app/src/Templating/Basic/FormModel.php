<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

/**
 * Description of FormModel
 *
 * @author user
 */
class FormModel {
    public $elementId;
    public $action;
    public $method;
    public $cssClasses;
    
    /**
     * fields, actions, summary, groups
     * @var array
     */
    public $children;
    
    public $htmlContent;
}
