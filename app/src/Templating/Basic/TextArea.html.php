<?php

use BlackStone\WebDef\Templating\Basic\TextAreaModel;

$d = new TextAreaModel();
$d = $data;

?>

<textarea id="<?php echo $d->elementId; ?>" name="<?php echo $d->name; ?>" rows="<?php echo $d->lines; ?>"><?php echo $d->value; ?></textarea>
