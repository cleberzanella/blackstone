<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of SelectTemplate
 *
 * @author user
 */
class SelectTemplate extends HtmlTemplate {
    
    /**
     *
     * @var SelectModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new SelectModel();
    }

    // substituir por dataSource
    public function addOption( $value, $description, $selected = null){
        
        $option = new SelectOptionModel();
        $option->value = $value;
        $option->description = $description;
        $option->selected = $selected == null ? false : $selected;
        
        array_push($this->data->options, $option);
    }
    
    public function render() {
        
        $this->data->elementId = "{$this->data->name}-field";
        
        return parent::render();
    }
}
