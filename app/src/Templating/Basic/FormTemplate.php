<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

use BlackStone\WebDef\Templating\HtmlTemplate;
use BlackStone\WebDef\Templating\Basic\Layout\GridContainerTrait;
use BlackStone\WebDef\Templating\Basic\FormModel;

/**
 * Description of FormTemplate
 *
 * @author user
 */
class FormTemplate extends HtmlTemplate {
    use GridContainerTrait;
    
    /**
     * @var FormModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new FormModel();
        $this->data->elementId = "form-id";
        $this->data->cssClasses = $this->getCssClasses() . " align-labels-top fill-inputs-horizontal";
    }
    
    
}
