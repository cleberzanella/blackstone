<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

/**
 * Description of SelectModel
 *
 * @author user
 */
class SelectModel {
    
    /**
     *
     * @var string
     */
    public $elementId;
    
    /**
     *
     * @var string
     */
    public $name;
    
    /**
     *
     * @var boolean
     */
    public $multiple;
    
    /**
     *
     * @var string
     */
    public $selectedValue;
    
    /**
     *
     * @var SelectOptionModel[]
     */
    public $options = array();
            
}
