<?php

use BlackStone\WebDef\Templating\Basic\FormModel;

$d = new FormModel();
$d = $data;

?>

<form action="<?php echo $d->action; ?>" method="<?php echo $d->method; ?>" id="<?php echo $d->elementId; ?>" class="<?php echo $d->cssClasses; ?>">
    <?php echo $d->htmlContent; ?>
</form>