<?php

use BlackStone\WebDef\Templating\Basic\ActionGroupModel;
use BlackStone\WebDef\Templating\Basic\ActionModel;

$d = new ActionGroupModel(); // põe tipagem na variável
$d = $data;

?>
            <span>
                <?php foreach($d->actions as $action): ?>
                <?php $tAction = new ActionModel(); $tAction = $action; ?>
                <button id="<?php echo $tAction->elementId; ?>" type="<?php echo $tAction->buttonType; ?>"><?php echo $tAction->label; ?></button>
                <?php endforeach;?>
            </span>
            
