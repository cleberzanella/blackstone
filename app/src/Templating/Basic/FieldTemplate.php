<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;


use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of LabelTemplate
 *
 * @author user
 */
class FieldTemplate extends HtmlTemplate {
    
    /**
     *
     * @var FieldModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new FieldModel();
    }
    
}
