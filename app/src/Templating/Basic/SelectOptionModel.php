<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

/**
 * Description of SelectOptionModel
 *
 * @author user
 */
class SelectOptionModel {

    public $value;
    
    public $description;
    
    /**
     *
     * @var boolean
     */
    public $selected;
    
    /**
     *
     * @var boolean
     */
    public $disabled;
    
}
