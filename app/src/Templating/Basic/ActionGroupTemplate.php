<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of ActionGroupTemplate
 *
 * @author user
 */
class ActionGroupTemplate extends HtmlTemplate {
    
    /**
     *
     * @var ActionGroupModel
     */
    public $data;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new ActionGroupModel();
    }

    public function addAction( $name, $label, $type = "button"){
        
        $actionBtn = new ActionModel();
        $actionBtn->name = $name;
        $actionBtn->label = $label;
        $actionBtn->elementId = "{$actionBtn->name}-btn";
        $actionBtn->buttonType = $type;
        
        array_push($this->data->actions, $actionBtn);
    }
    
}
