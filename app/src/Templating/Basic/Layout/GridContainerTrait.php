<?php

namespace BlackStone\WebDef\Templating\Basic\Layout;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of GridContainerMixin
 *
 * @author user
 */
trait GridContainerTrait {
    
    public function getCssClasses(){
        return "grid-container";
    }
    
    public function setGroup($columnsNumber){

        $group = new GroupTemplate();
        $group->setColumns($columnsNumber);
        
        parent::addChild($group);
    }
    
    public function addChild(HtmlTemplate $child){

        $lastGroup = parent::getLastChild();
        
        //adicona no grupo, e ainda decorado;
        $groupField = new GroupFieldTemplate();
        $groupField->setColumns(1);
        
        $groupField->addChild($child);
        $lastGroup->addChild($groupField);
    }
    
    // TODO : capacidade de adicionar um "filler" para onde se quer um espaço
}
