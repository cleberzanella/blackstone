<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic\Layout;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of GroupTemplate
 *
 * @author user
 */
class GroupTemplate extends HtmlTemplate {
    
    /**
     *
     * @var GroupFieldModel;
     */
    public $data;

    public function __construct() {
        parent::__construct();
        
        $this->data = new GroupFieldModel();
    }
    
    public function setColumns($num){
        switch ($num) {
            case 1:
                $this->data->columnsGroupClass = "fullwidth";
                break;
            default:
                $this->data->columnsGroupClass = "grid2";
                break;
        }
    }
    
}
