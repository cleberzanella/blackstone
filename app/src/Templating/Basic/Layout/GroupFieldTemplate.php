<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\Templating\Basic\Layout;

use BlackStone\WebDef\Templating\HtmlTemplate;

/**
 * Description of GroupFieldTemplate
 *
 * @author user
 */
class GroupFieldTemplate extends HtmlTemplate {
    
    /**
     *
     * @var GroupFieldModel;
     */
    public $data;

    public function __construct() {
        parent::__construct();
        
        $this->data = new GroupFieldModel();
        $this->data->columnsClass = "col"; // padrão
    }
    
    public function setColumns($num){
        // não suporta isso por enquanto
    }
    
}
