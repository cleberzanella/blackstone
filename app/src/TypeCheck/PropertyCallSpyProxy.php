<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlackStone\WebDef\TypeCheck;

/**
 * Description of PropertyCallSpyProxy
 *
 * @author user
 */
class PropertyCallSpyProxy {
    
    /**
     * @var \ReflectionClass
     */
    private $refClass;
    
    /**
     * @var PropertyCallInfo
     */
    public $lastCall;

    public function __construct($clazz)
    {
        $this->refClass = new \ReflectionClass($clazz);
    }
    
    public function __set($name, $value) { 
        
        $this->setV($name, $value);
    } 
    
    private function setV($name, $value) { 
        
        if(! $this->refClass->hasProperty($name)){
            throw new Exception("Class " . $this->refClass->getName() . " not has property " . $name); 
        } 
        
        $this->lastCall = new PropertyCallInfo($name, $this->getPropertyType($name), $value);
    } 
   
    public function __get($name) {
        $this->setV($name, null);
        
        return null;
    }

    public static function getPropertyType($parentClass, $propertyName){
        
        $reflector = new \ReflectionClass($parentClass);
        $refProperty = $reflector->getProperty($propertyName); 
        
        preg_match('/@var\s+([^\s]+)/', $refProperty->getDocComment(), $matches);
        
        if(count($matches) > 0){
            return $matches[1];
        }
        
        return null;
    }
    
}

class PropertyCallInfo {
    
    /**
     * @var string
     */
    public $name, $type, $value, $description2;
    
    public function __construct($n, $t, $v)
    {
        $this->name = $n;
        $this->type = $t;
        $this->value = $v;
    }
}
