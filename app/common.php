<?php

// ENV globals
define('BULLET_ENV', $request->env('BULLET_ENV', 'development'));
// Production setting switch
if(BULLET_ENV == 'production') {
    // Hide errors in production
    error_reporting(0);
    ini_set('display_errors', '0');
}

// Shortcut to access $app instance anywhere
function app() {
    global $app;
    return $app;
}

// Error logging
// http://stackoverflow.com/questions/3531703/how-to-log-errors-and-warnings-into-a-file

ini_set("log_errors", 0);
//ini_set("error_log", "/tmp/php-error.log");

// Display exceptions with error and 500 status
$app->on('Exception', function(\Bullet\Request $request, \Bullet\Response $response, \Exception $e) use($app) {
    if($request->format() === 'json') {
        $data = array(
            'error' => str_replace('Exception', '', get_class($e)),
            'message' => $e->getMessage()
        );
        // Debugging info for development ENV
        if(BULLET_ENV !== 'production') {
            $data['file'] = $e->getFile();
            $data['line'] = $e->getLine();
            $data['trace'] = $e->getTrace();
        }
        $response->content($data);
    } else {
        $response->content($app->template('errors/exception', array('e' => $e))->content());
    }
    //if(BULLET_ENV === 'production') {
        $errorType = str_replace('Exception', '', get_class($e));
        error_log($errorType.": ".($e->getMessage())." \n".($e->getTraceAsString()));
    //}
});

// Time Zone
 date_default_timezone_set("America/Sao_Paulo");

// Inicia o banco de dados
//use Data\DbInit;
//DbInit::initDatabase();

