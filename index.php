<?php

define('BULLET_ROOT', __DIR__);
define('BULLET_APP_ROOT', BULLET_ROOT . '/app/');
define('BULLET_SRC_ROOT', BULLET_APP_ROOT . '/src/');

// Em desenvolvimento
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// Composer Autoloader
$loader = require BULLET_ROOT . '/vendor/autoload.php';


// Bullet App init: https://github.com/vlucas/bulletphp-skeleton
$app = new Bullet\App(require BULLET_APP_ROOT . 'config.php');
$request = new Bullet\Request();

// Common include
require BULLET_APP_ROOT . '/common.php';

// Require all paths/routes
//$generalConfig = require 'config.php';

$routesDir = BULLET_APP_ROOT . '/routes/';
require $routesDir . 'api.php';


// Response
echo $app->run($request);
