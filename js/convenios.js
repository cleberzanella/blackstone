

    /*
     * Utilizar somente em casos onde se sabe que há poucos dados (menos de 80 registros).
     * Caso contrário é melhor utilizar um componente que envia o filtro para ser feito pelo do servidor.
     */
    var SmallDataSelectBox =  function(elementSelector, jsonUrl, jsonData) {
        var _selector = elementSelector;
        var _url = jsonUrl;
        var _data = jsonData;
        var _dependants = [];
            
        return {
                
        _refreshDependants: function() {
            
            var arrayLength = _dependants.length;
            for (var index = 0; index < arrayLength; index++) {
                if(this.getActualValues() == null){
                    // em caso de dependência, se o pai estiver sem valor, desabilita os filhos
                    _dependants[index].toggleDisabled(true);
                } else {
                    // se o valor do pai mudar, limpa e atualiza os filhos
                    _dependants[index].toggleDisabled(false);
                    _dependants[index].bindData();
                }
            }
        },

        _receiveData: function(data) {
            var selectField = $(_selector);
            selectField.empty();

            var defaultValue = selectField.attr("defaultvalue");
            var defaultValues = (defaultValue == null ? "" : defaultValue).split(";"); // TODO : suportar defaultSearchValues

            if(! selectField.prop('multiple')){
                selectField.append('<option disabled selected> -- selecione -- </option>');
            }

            $.each(data, function(index, item) {
                 function arrayContaints(itemsArray, itemCandidate) { return $.inArray(itemCandidate + '', itemsArray) >= 0; };
                 selectField.append( '<option value="' + item.value + '"' + ( arrayContaints(defaultValues, item.value) ? ' selected ' : '') + '>' + item.label + '</option>' );
            });

            if(selectField.attr("style") != null)
                // atualiza o chosen (já inicializou, pois está hidden)
                selectField.trigger("chosen:updated");
            else {
                var self = this;
                var f = function() {
                  self._refreshDependants(); // mantem o estado
                };
                
                // inicia o chosen
                selectField.chosen({allow_single_deselect:true, placeholder_text: "Selecione alguma opção"}).change(f);
            }
        },

        bindData: function() {
            var jsonDat = _data;
            if($.isFunction(jsonDat))
                jsonDat = jsonDat();
            
            var self = this;
            var f = function(dat) {
                self._receiveData(dat); // mantem o estado
            }
            
            $.getJSON(_url, jsonDat, f);
        },

        getActualValues : function() {
          return $(_selector + ' option:selected').val()  
        },
        
        toggleDisabled : function(disabled) {
            
           var foundElement = $(_selector);
            
          if(disabled)
              foundElement.attr('disabled', 'disabled');
          else
              foundElement.removeAttr('disabled');
        },
        
        addDependant : function(/*SearchBox*/ dependant){
          _dependants.push(dependant);
          this._refreshDependants();
        }
        
    };
 }

    var MaskedField = function(elementId, mask, unmaskValueFn){
      
      var _editorElementSelector = "#masked-" + elementId;
      var _valueElementSelector = "#" + elementId;
      var _unmaskValueFn = unmaskValueFn;
      var _mask = mask;
      
      var clazz = {
        unmaskValue: function(maskedValue) {
            return  _unmaskValueFn(maskedValue);
        },
        _onFocusOut: function(event){
            $(_valueElementSelector).val(clazz.unmaskValue($(event.target).val()));
        },
        init: function(){
            // copia os valores de um para outro caso existam
            $(_editorElementSelector).val($(_valueElementSelector).val());
            
            var component = $(_editorElementSelector).mask(_mask);

            component.focusout(clazz._onFocusOut);
        },
      };
      
      clazz.init();
      return clazz;
    };

    
    /***** Mask constants *****/
    {
        
        MaskedField.PLACEHOLDER = "_";
        
        MaskedField.ZIP_CODE_MASK = "99.999-999";
        MaskedField.ZI_CODE_UNMASK_FN = function(maskedValue){
            return maskedValue.replace(".", "").replace("-", "").replace(new RegExp(MaskedField.PLACEHOLDER, 'g'), "");
        };
        
        MaskedField.INTERNATIONAL_PHONE_MASK = "+99 (99) 9999-9999?9";
        MaskedField.INTERNATIONAL_PHONE_UNMASK_FN = function(maskedValue){
            return maskedValue.replace("+", "").replace("(", "").replace(")", "").replace("-","").replace(new RegExp(MaskedField.PLACEHOLDER, 'g'), "").replace(new RegExp(" ", 'g'), "");
        };
        
        MaskedField.CNPJ_MASK = "99.999.999/9999-99";
        MaskedField.CNPJ_UNMASK_FN = function(maskedValue){
            return maskedValue.replace(".", "").replace(".", "").replace("/", "").replace("-", "").replace(new RegExp(MaskedField.PLACEHOLDER, 'g'), "");
        };
    }

    // TODO : Crop da imagem (uma função "filter", que recebe um arquivo e retorna outro, ou ainda, cancela a seleção do arquivo.

    var ImageSelectBox =  function(valueInputSelector, fileInputSelector) {
        var _valueInputSelector = valueInputSelector;
        var _fileInputSelector = fileInputSelector;
        var _imageFilterFn = null;

        var clazz = {
            erase: function(){
                
                var listElem = $('#list', $(_fileInputSelector).parent());
                listElem.empty();
                
                // clean file info
                $(_valueInputSelector).val("");

                var fileInput = $(_fileInputSelector);
                fileInput.replaceWith( fileInput = fileInput.clone( true ) );
            },
            
            _renderThumbnail: function(base64Content, fileName){

                var listElem = $('#list', $(_fileInputSelector).parent());
                listElem.empty();

                // Render thumbnail.
                var newHtmlContent= 
                [
                  '<div style="position:relative">',
                  '<span id="close" onclick="" style="position:absolute; top:2px; left:2px; z-index:2; padding:2px 5px; background:#ccc;">x</span>',
                  '<img style="height: 55px; border: 1px solid #000; margin: 5px" src="', base64Content, '" title="', escape(fileName), '"/>',
                  '</div>'
                ].join('');

                listElem.html(newHtmlContent);

                $('#close', listElem).on("click", (function(control) {
                    return function(evt){
                        control.erase();

                        return false;
                    };
                })(this));
            },
            
            reset: function(){
                
                var self = this;
                
                function isNumeric( obj ) {
                    return !$.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
                }

                // verificar se o input tem valor, se tiver buscar a imagem do Server
                var fileId = $(_valueInputSelector).val();
                if(isNumeric(fileId)){

                    $.ajax({
                          url: "../api/files/raw/" + fileId,
                          type: "GET",
                          data: {},
                          success: function(base64Data) { 
                              // TODO : Tratar Not found
                              self._renderThumbnail(base64Data, ""); 
                          }
                    });

                } else {
                    this.erase();
                }
            },
            
            setFilterFn: function(filterFn){
                _imageFilterFn = filterFn;
            },
            
            _filterImage: function(initialBase64Content) {
                
                if(_imageFilterFn === null){
                    var defer = $.Deferred();
                    
                    defer.resolve(initialBase64Content);
                    
                    return defer.promise();
                }
                
                return _imageFilterFn(initialBase64Content);
            },
            
            // // copiado de http://stackoverflow.com/questions/14069421/show-an-image-preview-before-upload
            _onFileChange: function(evt) {

                var file = evt.target.files[0];

                if (!file.type.match('image.*')) {
                  return;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function(theFile, valueInputSelector, control) {
                  return function(e) {

                    // send image to server

                    var base64Content = e.target.result;

                    var jsonFile = {
                        'lastModified'     : theFile.lastModified,
                        'lastModifiedDate' : theFile.lastModifiedDate,
                        'name'             : theFile.name,
                        'size'             : theFile.size,
                        'type'             : theFile.type,
                         content           : base64Content
                    };
                    
                    var base64ContentPromise = control._filterImage(base64Content);

                    base64ContentPromise
                    .then(function(imgContent){
                    
                        control._renderThumbnail(imgContent, escape(jsonFile.name));

                        jsonFile.content = imgContent;
                                
                        $.ajax({
                              url: "../api/files/upload",
                              type: "POST",
                              data: jsonFile,
                              success: function(data) { 
                                  $(valueInputSelector).val(data.createdId);
                              }
                        });
                                
                    })
                    .fail(function(){
                       // o usuário cancelou o crop
                       control.reset(); 
                    });

                  };
                })(file, _valueInputSelector, this);

                // Read in the image file as a data URL.
                reader.readAsDataURL(file);
            },
            
            _init: function() {

                var self = this;
                self.reset();

                // vincula o "onChange"
                var f = function(evt) {
                    self._onFileChange(evt); // mantem o estado
                }
                $(_fileInputSelector).change(f);
            },

        };

        clazz._init();

        return clazz;
    };
    
    {
        ImageSelectBox.cropFilterFn = function(pxWidth, pxHeight){
            return function(base64Image){
                var div = $("<div/>");
                
                var dialogSize = Math.max(pxHeight, pxWidth) + 200;
                
                var uploadCroppie = div.croppie({
                    viewport: {
                        width: pxWidth,
                        height: pxHeight,
                    },
                    boundary: {
                        width: dialogSize,
                        height: dialogSize
                    }
                });
                
                uploadCroppie.croppie("bind", {
                    url: base64Image
                });
                
                var d = $.Deferred();
                
                div.dialog({
                    modal:true,
                    title:'Image cropping',
                    width: dialogSize + 20,
                    buttons: {
                        Cancelar: function() {
                            $( this ).dialog( "close" );
                        },
                        Ok: (function(deferred, crop) {
                           
                            return function() {
                                
                                var diag = $( this );
                                
                                crop.croppie("result", {type: 'canvas', size: 'original'})
                                        .then(function(base64Img) {
                                            deferred.resolve(base64Img);
                                    
                                            diag.dialog( "close" );
                                        });
                            };
                        })(d, uploadCroppie)
                    },
                    close: function(evt) {
                        d.reject();
                    }
                });
                
                return d.promise();
            };
        };
    }
            
    var MessageBox = function(containerId, severity, messages){
        var _containerId = containerId;
        var _severity = severity;
        var _messages = messages;

        return {
            _container: function(){
                return $("#" + _containerId);
            },
            _isString: function(s) {
                return typeof(s) === 'string' || s instanceof String;
            },
            clearContainer: function(){
                this._container().empty();
            },
            showBox: function(){

                if(this._isString(_messages) || _messages.length === 1){
                    
                    var val = this._isString(_messages) ? _messages : _messages[0];
                    
                    this._container().append('<div class="' + _severity + '">' + val + '</div>');
                    return;
                }
                
                var div = $("<div>");
                div.addClass(_severity);
                
                var ul = $("<ul>");
                
                for (var i = 0, l = _messages.length; i < l; ++i) {
                    ul.append('<li>' + _messages[i] + '</li>');
                }

                div.append(ul);
                this._container().append(div);
            },
            showBoxAlone: function(){
               this.clearContainer();
               this.showBox();
            }
        };
    };

    /***** MessageBox constants *****/
    {
        MessageBox.SUCCESS = "success";
        MessageBox.VALIDATION = "validation";
        MessageBox.ERROR = "error";
        MessageBox.show = function (containerId, severity, messages){
            var messageBox = new MessageBox(containerId, severity, messages);
            messageBox.showBoxAlone();
        };
        MessageBox.showValidation = function(containerId, messages){
            MessageBox.show(containerId, MessageBox.VALIDATION, messages);
        };
        MessageBox.showSuccess = function(containerId, messages){
            MessageBox.show(containerId, MessageBox.SUCCESS, messages);
        };
        MessageBox.showError = function(containerId, messages){
            MessageBox.show(containerId, MessageBox.ERROR, messages);
        };
    }


    var ValidationSummarizer = function(generalSummaryId, fieldMappings){
      var _generalSummaryContainerId = generalSummaryId;
      var _fieldMappings = fieldMappings;

      return {
          cleanFieldValidation: function(){
            // mensagens gerais
            var messageBox = new MessageBox(_generalSummaryContainerId, MessageBox.VALIDATION, []);
            messageBox.clearContainer();

            $.each(_fieldMappings, function(index, item) {
                $(".error-label", $(item.valueElementSelector).parent()).text("");
                $(".error-label", $(item.valueElementSelector).parent()).attr("title", "");
                $(".error-label", $(item.valueElementSelector).parent()).toggle(false);
                
                $(item.editionElementSelector).removeClass("error-field");
            });
          },
          showMessages: function(messages) {
            this.cleanFieldValidation();
            
            // show errors
            if(messages != null) {
                
                var generalMessages = [];
                
                $.each(messages, function(index, error) {
                    
                    if(! fieldMappings[error.fieldName]){
                        generalMessages.push(error.message);
                        return;
                    }
                    
                    var errorLabel = $(".error-label", $( fieldMappings[error.fieldName].valueElementSelector).parent());
                    errorLabel.text(errorLabel.text() + " " + error.message);
                    errorLabel.attr("title", errorLabel.text());

                    errorLabel.toggle(true);

                    $(fieldMappings[error.fieldName].editionElementSelector).addClass("error-field");
                });
                
                // mensagens gerais
                var messageBox = new MessageBox(_generalSummaryContainerId, MessageBox.VALIDATION, generalMessages);
                messageBox.clearContainer();

                if(generalMessages.length > 0){
                    messageBox.showBox();
                }
            }
          },
          getMessagesBoxContainerId: function(){
              return _generalSummaryContainerId;
          }
      };
      
    };

    var FormController = function (formComponentId, validationSummarizer){
        var _formComponent = $("#" + formComponentId);
        var _messageBoxContainerId = validationSummarizer.getMessagesBoxContainerId();
        var _validationSummarizer = validationSummarizer;
        var _closeUrl = null;
        
        var clazz = {
            setCloseUrl: function(closeUrl){
              _closeUrl = closeUrl;  
            },
            onSubmit: function(){
                
                
               var aform = _formComponent;

               // extrai os valores do form
               var values = {};
               var formArray = aform.serializeArray();
               $.each(formArray, function(i, field) {

                   if(field.name.endsWith("[ ]")){ // array
                       var fieldName = field.name.replace("[ ]", "");

                       if(!values[fieldName])
                           values[fieldName] = [];

                       values[fieldName].push(field.value);
                       return;
                   }

                   values[field.name] = field.value;
               });

               // envia para o servidor validar a ação
               $.ajax({
                   url: aform.attr("action"),
                   type: aform.attr("method"),
                   data: values,
                   success: function(resultJSON, status, jqXHR) {
                       
                       if(typeof resultJSON === "string"){
                           // erro no servidor PHP
                           // error de sintaxe do PHP retornam um 202 com uma página informando o erro
                           
                           if(console.error){
                                console.error(jqXHR.responseText);
                           }
                           
                           if(resultJSON.search("Fatal error") > -1){
                               MessageBox.showError(_messageBoxContainerId, "Ocorreu um erro inesperado ao executar a operação.");
                           }

                           return;
                       }
                       
                       _validationSummarizer.cleanFieldValidation();
                       
                       if(resultJSON.createdId){
                           // resultado de um Insert
                           setTimeout(function(){
                              document.location.href = '' + resultJSON.createdId;
                           }, 1500);
                       }
                       
                       MessageBox.showSuccess(_messageBoxContainerId, "Salvo com sucesso!");
                   },
                   error: function(jqXHR, textStatus, errorThrown){
                       
                       if(console.error){
                           console.error(jqXHR.responseText);
                       }
                       
                       switch(jqXHR.status){
                           case 400: // Bad Request
                               _validationSummarizer.showMessages(jqXHR.responseJSON); // exibe as validações
                               break;
                           case 500: // Server error
                               MessageBox.showError(_messageBoxContainerId, "Ocorreu um erro inesperado ao executar a operação.");
                               break;
                       }
                   }
               });
                
                
            },
            onClose: function(){
                document.location.href = _closeUrl;
            },
            init: function() {
                
                $("#close-btn", _formComponent).click( function(e){
                    clazz.onClose();
                });

                _formComponent.on("submit", function(e){
                    e.preventDefault();
                    
                    clazz.onSubmit();
                });
            }
        };
        
        clazz.init();
        return clazz;
    };